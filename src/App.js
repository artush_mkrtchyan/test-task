import React from 'react';
import { Provider } from 'react-redux';
import { configureStore } from './store/configure-store';
import Routes from './routes';
import ErrorBoundary from './errorBoundary';
import './App.css';

const store = configureStore();

function App() {
    return (
        <Provider store={store}>
            <ErrorBoundary>
                <Routes />
            </ErrorBoundary>
        </Provider>
    );
}

export default App;

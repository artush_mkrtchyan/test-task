import React from 'react';
import { Link } from 'react-router-dom';
import TasksList from './tasks';

const Home = () => {
    return (
        <div className="container">
            <div className="action-buttons">
                <Link to="/create">
                    <button>Create Task</button>
                </Link>
                <Link to="/login">
                    <button>Login</button>
                </Link>
            </div>
            <TasksList />
        </div>
    );
};

export default Home;

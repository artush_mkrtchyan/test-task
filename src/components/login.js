import React, { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { login } from '../service';

export default function Login() {
    const history = useHistory();
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState(null);

    useEffect(() => {
        localStorage.removeItem('token');
    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    const handleSubmit = (e) => {
        e.preventDefault();
        setError(null);
        const formData = new FormData();
        formData.append('username', username);
        formData.append('password', password);
        login(formData).then((res) => {
            if (res.status === 'ok') {
                localStorage.setItem('token', res.message.token);
                history.push('/');
            }
            if (res.status === 'error') {
                setError(res.message);
            }
        });
    };

    return (
        <div className="container">
            <div className="action-buttons">
                <Link to="/">
                    <button>Home</button>
                </Link>
            </div>
            <form onSubmit={handleSubmit}>
                <label htmlFor="username">User Name</label>
                <input
                    id="username"
                    value={username}
                    onChange={({ target: { value } }) => setUsername(value)}
                />
                <label htmlFor="password">Password</label>
                <input
                    id="password"
                    type="password"
                    value={password}
                    onChange={({ target: { value } }) => setPassword(value)}
                />
                <button>Submit</button>
            </form>
            {error &&
                Object.keys(error).map((el) => (
                    <div className="error-text text-center">{`${el}: ${error[el]}`}</div>
                ))}
        </div>
    );
}

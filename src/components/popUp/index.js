import React, { useEffect } from 'react';
import './index.css';

export default function PopUp(props) {
    const { onClose = () => {}, children } = props;

    useEffect(() => {
        document.addEventListener('keydown', handleKeyDown, false);
        return () => {
            document.removeEventListener('keydown', handleKeyDown, false);
        };
    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    const handleKeyDown = (e) => {
        if (e.keyCode === 27) {
            onClose();
        }
    };

    return (
        <div className="pop-up" onKeyDown={handleKeyDown}>
            <div className="close-icon" onClick={onClose}>
                X
            </div>
            {children}
        </div>
    );
}

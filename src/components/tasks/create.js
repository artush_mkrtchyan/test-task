import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { createTask } from '../../service';
import { validateEmail } from '../../utils';

const IBody = {
    username: '',
    email: '',
    text: '',
};

export default function CreateTask() {
    const [body, setBody] = useState(IBody);
    const [message, setMessage] = useState(null);
    const [isValidEmail, setValidEmail] = useState(true);

    const handleSubmit = (e) => {
        e.preventDefault();
        setMessage(null);
        const formData = new FormData();
        const { username, email, text } = body;

        formData.append('text', text);
        formData.append('username', username);
        formData.append('email', email);

        createTask(formData).then((res) => {
            if (res.status === 'ok') {
                setMessage({ '': 'success' });
                setBody(IBody);
            }
            if (res.status === 'error') {
                setMessage(res.message);
            }
        });
    };

    const handleChange = ({ target }) => {
        const { name, value } = target;
        if (name === 'email') {
            setValidEmail(validateEmail(value));
        }
        setBody((prev) => ({ ...prev, [name]: value }));
    };

    const { username, email, text } = body;
    return (
        <div className="container">
            <div className="action-buttons">
                <Link to="/">
                    <button>Home</button>
                </Link>
            </div>
            <form onSubmit={handleSubmit}>
                <label htmlFor="username">User Name</label>
                <input
                    id="username"
                    name="username"
                    value={username}
                    onChange={handleChange}
                />
                <label htmlFor="email">Email</label>
                <input
                    id="email"
                    name="email"
                    type="email"
                    value={email}
                    onChange={handleChange}
                />
                {!isValidEmail && (
                    <div className="validation-error">
                        Please enter a valid email address
                    </div>
                )}
                <label htmlFor="text">Text</label>
                <input
                    id="text"
                    name="text"
                    value={text}
                    onChange={handleChange}
                />
                <button disabled={!isValidEmail}>Submit</button>
            </form>
            {message &&
                Object.keys(message).map((el, idx) => (
                    <div
                        key={idx}
                        className="error-text text-center"
                    >{`${el} ${message[el]}`}</div>
                ))}
        </div>
    );
}

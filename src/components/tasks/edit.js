import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { editTaskStart } from '../../redux/tasks/action';
import PopUp from '../popUp';
import { TASK_STATUS } from '../../utils/constants';

export default function EditTask({ task = {}, onClose = () => {} }) {
    const history = useHistory();
    const dispatch = useDispatch();
    const [text, setText] = useState('');
    const [status, setStatus] = useState(0);

    useEffect(() => {
        if (task && task.id) {
            setText(task.text);
            setStatus(task.status);
        }
    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    const handleSubmit = (e) => {
        e.preventDefault();
        const formData = new FormData();

        formData.append('text', text);
        formData.append('status', status);
        formData.append('token', localStorage.getItem('token'));

        dispatch(editTaskStart(task.id, formData, history));
    };

    return (
        <PopUp onClose={onClose}>
            <div className="container">
                <div className="edit-form">
                    <form onSubmit={handleSubmit}>
                        <label htmlFor="text">Text</label>
                        <input
                            id="text"
                            value={text}
                            onChange={({ target }) => setText(target.value)}
                        />
                        <label htmlFor="status">Text</label>
                        <select
                            value={status}
                            onChange={({ target }) => setStatus(target.value)}
                        >
                            {Object.keys(TASK_STATUS).map((key, idx) => (
                                <option key={idx} value={key}>
                                    {TASK_STATUS[key]}
                                </option>
                            ))}
                        </select>
                        <button>Submit</button>
                    </form>
                </div>
            </div>
        </PopUp>
    );
}

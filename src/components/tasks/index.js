import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Pagination from '../pegination';
import EditTask from './edit';
import { getTasksStart } from '../../redux/tasks/action';
import './index.css';
import { TASK_STATUS } from '../../utils/constants';

const TasksList = () => {
    const dispatch = useDispatch();
    const { tasks: { message = {} } = {}, isEdited } = useSelector(
        (state) => state.tasks,
    );
    const { tasks = [], total_task_count = 0 } = message;
    const pageCount = total_task_count / 3;
    const [page, setPage] = useState(1);
    const [selectedTask, setSelectedTask] = useState(null);
    const [sortField, setSortField] = useState('');
    const [sortDirection, setSortDirection] = useState('asc');
    const isLogedin = localStorage.getItem('token');

    useEffect(() => {
        dispatch(getTasksStart({ page }));
    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    useEffect(() => {
        if (isEdited) {
            dispatch(
                getTasksStart({
                    page,
                    sort_field: sortField,
                    sort_direction: sortDirection,
                }),
            );
            setSelectedTask(null);
        }
    }, [isEdited]); // eslint-disable-line react-hooks/exhaustive-deps

    const handelPageChange = ({ selected }) => {
        setPage(selected + 1);
        dispatch(
            getTasksStart({
                page: selected + 1,
                sort_field: sortField,
                sort_direction: sortDirection,
            }),
        );
    };

    const handleSort = (sort_field) => () => {
        const sort_direction = sortDirection === 'asc' ? 'desc' : 'asc';
        setSortField(sort_field);
        setSortDirection(sort_direction);
        dispatch(getTasksStart({ page, sort_field, sort_direction }));
    };

    return (
        <div>
            <table>
                <thead>
                    <tr>
                        <th
                            className="sort-icon"
                            onClick={handleSort('username')}
                        >
                            Name
                        </th>
                        <th className="sort-icon" onClick={handleSort('email')}>
                            Email
                        </th>
                        <th>Text</th>
                        <th
                            className="sort-icon"
                            onClick={handleSort('status')}
                        >
                            Status
                        </th>
                        {isLogedin && <th />}
                    </tr>
                </thead>
                <tbody>
                    {tasks.map((item) => {
                        return (
                            <tr key={item.id}>
                                <td>{item.username}</td>
                                <td>{item.email}</td>
                                <td>{item.text}</td>
                                <td>{TASK_STATUS[item.status]}</td>
                                {isLogedin && (
                                    <td onClick={() => setSelectedTask(item)}>
                                        edit
                                    </td>
                                )}
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            {pageCount > 1 && (
                <Pagination
                    onPageChange={handelPageChange}
                    pageCount={pageCount}
                />
            )}
            {selectedTask && (
                <EditTask
                    task={selectedTask}
                    onClose={() => setSelectedTask(null)}
                />
            )}
        </div>
    );
};

export default TasksList;

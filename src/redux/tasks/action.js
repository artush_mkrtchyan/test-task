import { tasksTypes } from './type';

export const getTasksStart = (payload) => ({
    type: tasksTypes.GET_TASKS_START,
    payload,
});

export const getTasksSuccess = (data) => ({
    type: tasksTypes.GET_TASKS_SUCCESS,
    data,
});

export const getTasksFailure = (error) => ({
    type: tasksTypes.GET_TASKS_FAILURE,
    error,
});

export const editTaskStart = (id, body, history) => ({
    type: tasksTypes.EDIT_TASK_START,
    id,
    body,
    history
});

export const editTaskSuccess = (data) => ({
    type: tasksTypes.EDIT_TASK_SUCCESS,
    data,
});

export const editTaskFailure = (error) => ({
    type: tasksTypes.EDIT_TASK_FAILURE,
    error,
});

import { tasksTypes } from './type';

const initialState = {
    tasks: [],
    isLoading: false,
    isEdited: false,
};

export const tasks = (state = initialState, action) => {
    switch (action.type) {
        case tasksTypes.GET_TASKS_START:
            return { ...state, isLoading: true, isEdited: false };
        case tasksTypes.GET_TASKS_SUCCESS:
            return { ...state, tasks: action.data, isLoading: false };
        case tasksTypes.GET_TASKS_FAILURE:
            return { ...state, tasks: [], isLoading: false };
        case tasksTypes.EDIT_TASK_START:
            return { ...state, isEdited: false };
        case tasksTypes.EDIT_TASK_SUCCESS:
            return { ...state, isEdited: true };
        case tasksTypes.EDIT_TASK_FAILURE:
            return { ...state, isEdited: false };
        default:
            return state;
    }
};

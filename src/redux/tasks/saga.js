import { put, call, takeLatest } from 'redux-saga/effects';
import * as service from '../../service';
import {
    getTasksSuccess,
    getTasksFailure,
    editTaskSuccess,
    editTaskFailure,
} from './action';
import { tasksTypes } from './type';

export function* getTasks({ payload = {} }) {
    const { page = 1, sort_field, sort_direction } = payload;
    try {
        const data = yield call(() => {
            return service.fetchTasks(page, sort_field, sort_direction);
        });
        yield put(getTasksSuccess(data));
    } catch (e) {
        yield put(getTasksFailure(e.message));
    }
}

export function* editTask({ id, body, history }) {
    try {
        const data = yield call(() => {
            return service.editTask(id, body);
        });
        if (data.status === 'ok') {
            yield put(editTaskSuccess(data));
        }
        if (data.message && data.message.token && history) {
            history.push('/login');
        }
    } catch (e) {
        yield put(editTaskFailure(e.message));
    }
}

export function* tasks() {
    yield takeLatest(tasksTypes.GET_TASKS_START, getTasks);
    yield takeLatest(tasksTypes.EDIT_TASK_START, editTask);
}

export default tasks;

import React from 'react';
import {
    BrowserRouter as Router,
    Route,
    Switch,
    Redirect,
} from 'react-router-dom';
import Home from '../components';
import Login from '../components/login.js';
import CreateTask from '../components/tasks/create.js';
import NotFound from '../components/404';

const Routes = () => {
    return (
        <Router>
            <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/login" component={Login} />
                <Route exact path="/create" component={CreateTask} />
                <Route
                    path="/404"
                    render={(props) => <NotFound {...props} />}
                />
                <Redirect from="*" to="/404" />
            </Switch>
        </Router>
    );
};

export default Routes;

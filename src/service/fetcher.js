import axios from 'axios';
import cnf from '../confige/confige';

export const fetcher = (url, method = 'GET', data = {}) => {
    if (method.toLowerCase() === 'get') {
        return axios.get(cnf.apiUrl + url).then((res) => res.data);
    }

    let body = JSON.stringify(data);

    if (method.toLowerCase() === 'post') {
        body = data;
    }

    return axios[method.toLowerCase()](cnf.apiUrl + url, body).then(
        (res) => res.data,
    );
};

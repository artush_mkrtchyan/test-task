import { fetcher } from './fetcher';
import cnf from '../confige/confige.json';

export const fetchTasks = (page = 1, sort_field, sort_direction) =>
    fetcher(
        `?developer=${cnf.devName}&page=${page}${
            sort_field
                ? `&sort_field=${sort_field}&sort_direction=${sort_direction}`
                : ''
        }`,
    );

export const createTask = (body) =>
    fetcher(`create/?developer=${cnf.devName}`, 'post', body);

export const editTask = (id, body) =>
    fetcher(`edit/${id}/?developer=${cnf.devName}`, 'post', body);

export const login = (body) =>
    fetcher(`login/?developer=${cnf.devName}`, 'post', body);

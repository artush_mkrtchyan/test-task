import { combineReducers } from 'redux';
import { tasks } from '../redux/tasks/reducer';

const rootReducer = combineReducers({
    tasks,
});

export default rootReducer;

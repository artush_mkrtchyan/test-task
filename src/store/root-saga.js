import { all, spawn } from 'redux-saga/effects';
import { tasks } from '../redux/tasks/saga';

export default function* rootSaga() {
    yield all([spawn(tasks)]);
}
